import { combineReducers } from 'redux';

import category from './category.js';
import product from './product.js';

export default combineReducers({
  category,
  product,
});
