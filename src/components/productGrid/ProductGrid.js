// @flow
import React from 'react';
import ProductTile from './../productTile';

type ProductGridProps = {
    products: Array<any>
}

const ProductGrid = ({ products }: ProductGridProps) => (<div className="row">
        {
            products.map(product => <div className="col-xs-12 col-md-4" key={product.id}>
                    <ProductTile product={product} withShowMore={'yes'} />
                </div>)
        }
    </div>);

export default ProductGrid;
