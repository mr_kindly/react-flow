// @flow
import React from 'react';
import classnames from 'classnames';

type PriceProps = {
    value: number;
    discount: number;
    currency: string;
}

const calculatePrice = (price: number, discount: number):number => ((price * (100 - discount)) / 100);


const Price = (props: PriceProps) => {
  const { value, discount, currency } = props;
  const price = calculatePrice(value, discount);
  const priceStyle = classnames({
    price: true,
    'price--crossed': !!discount,
  });

  return (
        <h4>
            <span className={priceStyle}>
                {value}{currency}
            </span>
            {!!discount && <span className="price--discont"> {price}{currency} </span>}
        </h4>);
};

export default Price;
